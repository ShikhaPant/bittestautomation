package com.genpact.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GooglePage {
	
	WebDriver d;
	
	@FindBy(xpath=".//*[@id='hplogo']")
	public WebElement GoogleLogo;
	
	@FindBy(xpath=".//*[@id='gs_htif0']")
	public WebElement InputTextbox;
	
	public GooglePage(WebDriver df){
		
		d=df;
	PageFactory.initElements(d, this);
	}
	public void searchKeyword(){
		
		InputTextbox.sendKeys("what is trending");	
	}

}
