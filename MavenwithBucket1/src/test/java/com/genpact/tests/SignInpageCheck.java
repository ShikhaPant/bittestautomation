package com.genpact.tests;

import static org.testng.Assert.assertFalse;

import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.genpact.pages.GooglePage;
import com.genpact.utilities.CoreUtilities;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import jxl.common.Logger;;

public class SignInpageCheck extends CoreUtilities {

	public WebDriver driver;
	Logger log = Logger.getLogger(SignInpageCheck.class);
	GooglePage page1;
	ExtentReports extentreport;
	ExtentTest logger;

	@BeforeClass()
	public void setup() throws Throwable {
		try {

			BasicConfigurator.configure();
			log.info("************************************");
			log.info("Starting executing " + Thread.currentThread().getStackTrace()[1].getClassName() + " Testcases");
			log.info("************************************");
			// Report Directory and Report Name
			extentreport = new ExtentReports(System.getProperty("user.dir") + "\\test-output\\GoodReport.html", true);

			extentreport.loadConfig(new File(System.getProperty("user.dir") + "\\ExentFilesConfig\\extent-config.xml"));
			extentreport.addSystemInfo("Environment", "QA-Sanity");
			System.setProperty("webdriver.gecko.driver", "DriverExe//geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver",
			// "E:\\Eclipse_Mars_WIN\\test\\AutomationTest\\src\\main\\geckodriver.exe");
			String url = readProperty("Environment_URL");
			System.out.println(url);
			driver = new FirefoxDriver();
			page1 = new GooglePage(driver);
			driver.get(url);
			Thread.sleep(8000);

			logger = extentreport.startTest("befreclass[ nota test method");
			logger.log(LogStatus.INFO, "******************************");

			logger.log(LogStatus.INFO, "Started the automation suite now");
			logger.log(LogStatus.INFO, "******************************");

			extentreport.endTest(logger);

		} catch (Throwable e) {
			log.error(e);
			throw e;
		}

	}

	@Test
	public void assertions() throws Throwable {

		try {
			logger = extentreport.startTest("Assertions");
			// logger.log(LogStatus.INFO, "Strating to execute test");

			// logger.log(LogStatus.FAIL, "Assert Fail as condition is False");
			assertFalse(page1.GoogleLogo.isDisplayed(), "Logo is not displayed");
			extentreport.endTest(logger);

		}

		catch (Throwable e) {
			takescreenshot(Thread.currentThread().getStackTrace()[1].getMethodName(), driver);
			log.error(e);
			logger.log(LogStatus.ERROR, "some exception occurred");
			logger.log(LogStatus.FAIL, e);

	//		logger.log(LogStatus.FAIL, "Snapshot below: " + log.addScreenCapture(destfil.getAbsolutePath())));
		
			logger.log(LogStatus.FAIL, "Snapshot below: " + logger.addScreenCapture(destfil.getAbsolutePath()));
			throw e;
		}
	}

	@Test(enabled = false)
	public void searchText() throws Throwable {
		try {
			extentreport.startTest("starting test2");

			page1.searchKeyword();
		}

		catch (Throwable e) {
			log.error(e);
			takescreenshot(Thread.currentThread().getStackTrace()[1].getMethodName(), driver);

			throw e;

		}
	}

	@AfterClass()
	public void clear() throws InterruptedException {
		Thread.sleep(7000);
		driver.quit();
		extentreport.flush();

		extentreport.close();

	}
}
