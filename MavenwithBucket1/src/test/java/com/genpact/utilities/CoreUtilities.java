package com.genpact.utilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CoreUtilities {

	public Properties p = new Properties();
	String Proertyname = "config.properties";
	public WebDriver coredriver;
	public File destfil;
	public void takescreenshot(String screenshotname, WebDriver scd) throws Throwable {
		try {
			coredriver = scd;
			TakesScreenshot screen = (TakesScreenshot) coredriver;
			File src = screen.getScreenshotAs(OutputType.FILE);
			 destfil = new File("Screenshots\\" + screenshotname);
			FileUtils.copyFile(src, destfil);

		}

		catch (Throwable e) {
			throw e;
		}
	}

	public String readProperty(String pname) throws IOException {

		InputStream inpt = getClass().getClassLoader().getResourceAsStream(Proertyname);
		p.load(inpt);
		String url = p.getProperty("Environment_URL");
		return url;
	}

}
