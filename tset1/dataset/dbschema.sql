-- --------------------------------------------------------
-- Host:                         10.10.12.23
-- Server version:               5.6.10 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for test_results
DROP DATABASE IF EXISTS `test_results`;
CREATE DATABASE IF NOT EXISTS `test_results` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test_results`;


-- Dumping structure for table test_results.phm_results
DROP TABLE IF EXISTS `phm_results`;
CREATE TABLE IF NOT EXISTS `phm_results` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` int(11) DEFAULT '0',
  `TESTCASE_NAME` varchar(500) DEFAULT NULL,
  `STATUS` varchar(20) DEFAULT NULL,
  `TIMETAKEN` varchar(20) DEFAULT NULL,
  `MESSAGE` varchar(2000) DEFAULT NULL,
  `ENVIRONMENT_NAME` varchar(500) DEFAULT NULL,
  `INSERT_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_TIME` timestamp NOT NULL DEFAULT '1971-01-01 01:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `phm_results` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
