<%@ page language="java" contentType="text/html; 1charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="refresh" content="30">
<title>PHM Reports</title>
<style type="text/css">
.wraptext1 {
	font-weight: bold;
}

.wraptext {
	word-wrap: break-word;
}

.CSSTableGenerator {
	font-family: Arial, Helvetica, sans-serif;
	color: #333333;
	font-size: 11px;
	font-weight: normal;
	margin: 0px;
	padding: 0px;
	width: 100%;
}

.CSSTableGenerator table {
	table-layout: fixed;
	border-collapse: collapse;
	border-spacing: 0;
	/*width: 100%;*/
	height: 100%;
	margin: 0px;
	padding: 0px;
}

.CSSTableGenerator tr.yellow>td {
	background-color: #fffebe;
	color: #333333;
}

.CSSTableGenerator tr.red>td {
	background-color: #fedddd;
	color: #860309;
}

.CSSTableGenerator tr.grey>td {
	background-color: #e6ebf0;
	color: #333333;
	text-decoration: line-through;
}

.CSSTableGenerator td {
	word-wrap: break-word;
	vertical-align: middle;
	background-color: #dff5d0;
	color: #285610;
	border: 1px solid #989898;
	text-align: center;
	padding: 10px;
}

.CSSTableGenerator th {
	word-wrap: break-word;
	vertical-align: middle;
	color: #333333;
	border: 1px solid #989898;
	text-align: center;
	padding: 10px;
}

.CSSTableGenerator tr.header>td {
	background-color: #f8f8f8;
	text-align: center;
	font-size: 11px;
	font-weight: bold;
	color: #333333;
	border-bottom: none;
}

.CSSTableGenerator tr.header>td select {
	font-size: 11px;
	color: #333333;
	height: 30px;
	line-height: 28px;
}

.CSSTableGenerator td button {
	background: #fff;
	font-size: 10px;
	border: 1px solid #989898;
}
</style>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
	 // window.location = 'listrecords.jsp?environment=PRODUCTION&componentName=EMBEDDED&status=FAILURE';

function changes(id){
	$('#acknowledgement'+id).show();
	$('#acknowledgementmsg'+id).show();
	$('#testdesc'+id).show();
	$('#disablealert'+id).show();
}
$(function(){
    // bind change event to select
    $('#componentName').on('change', function () {
        var status =$("#status").val();
        var environment =$("#environment").val();
        var componentName = $(this).val(); // get selected value
        if (componentName) { // require a URL
            window.location = 'index.jsp?componentName='+componentName+'&environment='+environment+'&status='+status; 
        }
        return false;
    });
    $('#environment').on('change', function () {
        var environment = $(this).val(); // get selected value
        if (environment) { // require a URL
        	var status=$("#status").val();
        	var componentName=$("#componentName").val(); 
            window.location = 'index.jsp?componentName='+componentName+'&environment='+environment+'&status='+status; 
        }
        return false;
    });
    $('#status').on('change', function () {
        var status = $(this).val(); // get selected value
        if (status) { // require a URL
        	var componentName=$("#componentName").val();
        	var environment=$("#environment").val(); 
            window.location = 'index.jsp?componentName='+componentName+'&environment='+environment+'&status='+status; 
        }
        return false;
    });
  });

</script>
</head>
<body>
	<%
		String status = "";
		String environment = "";
		String componentName = "";
		String query = "";
		if (null == request.getParameter("componentName") && null == request.getParameter("environment") && null == request.getParameter("status")) {
			request.setAttribute("componentName", "All");
			request.setAttribute("environment", "PRODUCTION");
			request.setAttribute("status", "FAILURE");

		}
		//out.println("getparam"+request.getParameter("componentName"));
		//out.println("getatt:-"+request.getAttribute("componentName"));

		if (null != request.getAttribute("componentName")) {
			componentName = (String) request.getAttribute("componentName");
			status = (String) request.getAttribute("status");
			environment = (String) request.getAttribute("environment");
		} else {
			componentName = request.getParameter("componentName");
			status = request.getParameter("status");
			environment = request.getParameter("environment");

		}

		if (componentName != null && componentName.equalsIgnoreCase("All")) {
			query = "select ID,TESTCASE_NAME,TESTCASE_DESCRIPTION,COMPONENT_NAME,STATUS,ENVIRONMENT_NAME,MESSAGE,REPORT_URL,SCREENSHOT_URL,ACKNOWLEDGEMENT,"
					+ "ACKNOWLEDGEMENT_MESSAGE,DISABLED,UPDATE_TIME" + " from phm_results_master where " + " status='" + status + "' and environment_name='" + environment
					+ "' order by update_time desc";
		} else {
			query = "select ID,TESTCASE_NAME,TESTCASE_DESCRIPTION,COMPONENT_NAME,STATUS,ENVIRONMENT_NAME,MESSAGE,REPORT_URL,SCREENSHOT_URL,ACKNOWLEDGEMENT,"
					+ "ACKNOWLEDGEMENT_MESSAGE,DISABLED,UPDATE_TIME" + " from phm_results_master where " + " status='" + status + "' and COMPONENT_NAME='" + componentName
					+ "' and environment_name='" + environment + "' order by update_time desc";
		}
		//out.print(query);

		//select ID,TESTCASE_NAME,TESTCASE_DESCRIPTION,COMPONENT_NAME,STATUS,ENVIRONMENT_NAME,MESSAGE,REPORT_URL,SCREENSHOT_URL,ACKNOWLEDGEMENT,ACKNOWLEDGEMENT_MESSAGE,DISABLED,UPDATE_TIME from phm_results_master where status='FAILURE' and environment_name='PRODUCTION' or (status='SUCCESS' and environment_name='PRODUCTION' and disabled=1);
	%>
	<sql:setDataSource var="myDS" driver="com.mysql.jdbc.Driver"
		url="jdbc:mysql://10.10.12.23:3306/test_results" user="root"
		password="Q@te@m" />

	<sql:query var="listUsers" dataSource="${myDS}">
		<%=query%>;
	</sql:query>

	<div align="center" class="CSSTableGenerator">
		<caption>
			<h2>PHM Reports</h2>
		</caption>
		<table width="80%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr class="header">
				<td align="center">Environment :<select name="environment"
					ID="environment">
						<option
							<%if (null != request.getParameter("environment") && request.getParameter("environment").equalsIgnoreCase("PRODUCTION")) {%>
							selected <%}%>>PRODUCTION</option>
						<option
							<%if (null != request.getParameter("environment") && request.getParameter("environment").equalsIgnoreCase("STAGING")) {%>
							selected <%}%>>STAGING</option>
						<option
							<%if (null != request.getParameter("environment") && request.getParameter("environment").equalsIgnoreCase("QA")) {%>
							selected <%}%>>QA</option>
				</select>
				</td>
				<td align="center">Component Name :<select name="componentName"
					id="componentName">
						<option
							<%if (null != request.getParameter("componentName") && request.getParameter("componentName").equalsIgnoreCase("All")) {%>
							selected <%}%>>All</option>
						<option
							<%if (null != request.getParameter("componentName") && request.getParameter("componentName").equalsIgnoreCase("EMBEDDED")) {%>
							selected <%}%>>Embedded</option>
						<option
							<%if (null != request.getParameter("componentName") && request.getParameter("componentName").equalsIgnoreCase("ORION")) {%>
							selected <%}%>>Orion</option>
						<option
							<%if (null != request.getParameter("componentName") && request.getParameter("componentName").equalsIgnoreCase("PORTAL")) {%>
							selected <%}%>>Portal</option>
						<option
							<%if (null != request.getParameter("componentName") && request.getParameter("componentName").equalsIgnoreCase("DNB")) {%>
							selected <%}%>>DnB</option>
				</select>
				</td>
				<td align="center">Status :<select name="status" id="status">
						<option
							<%if (null != request.getParameter("status") && request.getParameter("status").equalsIgnoreCase("FAILURE")) {%>
							selected <%}%>>Failure</option>
						<option
							<%if (null != request.getParameter("status") && request.getParameter("status").equalsIgnoreCase("SUCCESS")) {%>
							selected <%}%>>Success</option>
				</select>
				</td>
			</tr>
		</table>
		<table width="100%" align="center" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<th width="4%">ID</th>
				<th width="9%">TestCase Name</th>
				<th width="8%">TestCase Details</th>
				<th width="6%">Component</th>
				<th width="5%">Status</th>
				<th width="11%">Failure Message</th>
				<th width="4%">Report</th>
				<th width="6%">Screenshot</th>
				<th width="8%">Last Failure at</th>
			</tr>
			<c:forEach var="phm_results_master" items="${listUsers.rows}">
				<tr
					<c:choose>
                               <c:when test="${phm_results_master.STATUS == 'SUCCESS' && phm_results_master.DISABLED== true}">class="green"</c:when>
                               <c:when test="${phm_results_master.STATUS == 'FAILURE' && phm_results_master.DISABLED== false && phm_results_master.ACKNOWLEDGEMENT == false}">class="red"</c:when>
                      </c:choose>>

					<td><c:out value="${phm_results_master.ID}" /></td>
					<td class="wraptext1"><c:out
							value="${phm_results_master.TESTCASE_NAME}" /></td>
					<td><c:out value="${phm_results_master.TESTCASE_DESCRIPTION}" />
						<div id="testdesc<c:out value="${phm_results_master.ID}" />"
							style="display: none;">
						</div></td>
					<td><c:out value="${phm_results_master.COMPONENT_NAME}" /></td>
					<td><c:out value="${phm_results_master.STATUS}" /></td>
					<td class="wraptext"><c:out
							value="${phm_results_master.MESSAGE}" /></td>
					<td><a
						href="<c:out value='${phm_results_master.REPORT_URL}'/>"
						target='_new'>Link</a></td>
					<td><a
						href="<c:out value='${phm_results_master.SCREENSHOT_URL}'/>"
						target='_new'>Link</a></td>
					<td><c:out value="${phm_results_master.UPDATE_TIME}" /></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>