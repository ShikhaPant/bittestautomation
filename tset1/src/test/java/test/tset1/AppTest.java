package test.tset1;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author spant
 */
public class AppTest {

	static String divpresent = "//a[contains(text(),'Sign In')]";
	public static WebDriver d;

	@BeforeClass
	public static void SignUpPageOpen() throws Throwable {
		try {

			System.setProperty("webdriver.gecko.driver",
					"E:\\Eclipse_Mars_WIN\\test\\AutomationTest\\src\\test\\java\\driverexe\\geckodriver.exe");
			d = new FirefoxDriver();
			d.get("http://www.guru99.com/");
			System.out.println("in before cla  yesss");

		} catch (Throwable e) {
			throw e;
		}
	}

	@BeforeMethod
	public static void testbefore() {
		System.out.println("in before method");

	}
	
	@AfterMethod
	public static void aftermethod() {
		System.out.println("in after method");
		assertTrue(true);


	}
	

	@AfterClass
	public static void afterclass() {
		System.out.println("in after class");

	}
	
	@Test
	public static void test3() throws Throwable {
		try {

			System.out.println("in test 3");

		} catch (Throwable e) {
			throw e;
		}
	}

	@Test
	public static void Wtest2() throws Throwable {
		try {

			System.out.println("in test 2");

		} catch (Throwable e) {
			throw e;
		}
	}

	@Test(dependsOnMethods = { "test3", "Wtest2" })
	public static void test4() throws Throwable {
		try {

			System.out.println("in test 4");

		} catch (Throwable e) {
			throw e;
		}
	}

}
