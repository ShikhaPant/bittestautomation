package test.tset1;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class SimpleTest 
{
	
	
	@DataProvider(name = "excelsheet1")
	public Object[][] createData2() throws Exception  {
		Object[][] retObjArr = getTableArray("dataset/" + "LoginURL.xls", "Login", "TableStart", "TableEnd");
		return (retObjArr);
	}

	public String[][] getTableArray(String xlFilePath, String sheetName, String tableName, String tableNameEnd) throws BiffException, IOException  {
		String[][] tabArray = null;

		Workbook workbook = Workbook.getWorkbook(new File(xlFilePath));
		Sheet sheet = workbook.getSheet(sheetName);
		int startRow, startCol, endRow, endCol, ci, cj;
		Cell tableStart = sheet.findCell(tableName);
		startRow = tableStart.getRow();
		startCol = tableStart.getColumn();
		Cell tableEnd = sheet.findCell(tableNameEnd);// Getting the end of
														// desired table by
														// identifying its
														// cell
		endRow = tableEnd.getRow();
		endCol = tableEnd.getColumn();
		System.out.println("startRow=" + startRow + ", endRow=" + endRow + ", " + "startCol=" + startCol + ", endCol=" + endCol);
		tabArray = new String[endRow - startRow - 1][endCol - startCol - 1];
		ci = 0;

		for (int i = startRow + 1; i < endRow; i++, ci++) {
			cj = 0;
			for (int j = startCol + 1; j < endCol; j++, cj++) {
				tabArray[ci][cj] = sheet.getCell(j, i).getContents();
			}
		}
		return (tabArray);
	}
    
    @Test(dataProvider="excelsheet1",groups="t")
    public void simpleTest(String URL,String assertion,String message) {
       assertTrue(false);
       System.out.println(URL+ " " + assertion+ " " + message);
    }
}
 
 